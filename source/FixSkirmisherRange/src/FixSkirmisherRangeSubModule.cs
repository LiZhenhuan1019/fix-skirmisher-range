﻿using FixSkirmisherRange.Query;
using HarmonyLib;
using TaleWorlds.Core;
using TaleWorlds.MountAndBlade;

namespace FixSkirmisherRange
{
    public class FixSkirmisherRangeSubModule : MBSubModuleBase
    {
        private bool _firstTime = true;
        private bool _success = true;

        protected override void OnSubModuleLoad()
        {
            base.OnSubModuleLoad();

            var harmony = new Harmony(nameof(FixSkirmisherRangeSubModule));
            _success &= Patch_MissionWeapon.Patch(harmony);
            _success &= Patch_MovementOrder.Patch(harmony);
        }

        protected override void OnBeforeInitialModuleScreenSetAsRoot()
        {
            base.OnBeforeInitialModuleScreenSetAsRoot();

            if (!_firstTime)
                return;

            _firstTime = false;
            if (!_success)
                InformationManager.DisplayMessage(new InformationMessage(nameof(FixSkirmisherRangeSubModule) + " patch failed"));
        }
    }
}
