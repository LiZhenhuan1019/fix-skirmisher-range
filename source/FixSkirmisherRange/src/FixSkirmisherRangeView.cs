﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FixSkirmisherRange.Query;
using TaleWorlds.MountAndBlade;
using TaleWorlds.MountAndBlade.View.Missions;

namespace FixSkirmisherRange
{
    [DefaultView]
    public class FixSkirmisherRangeView : MissionView
    {
        public override void OnBehaviourInitialize()
        {
            base.OnBehaviourInitialize();

            QueryDataStore.EnsureInitialized();
        }

        public override void OnRemoveBehaviour()
        {
            base.OnRemoveBehaviour();

            QueryDataStore.Clear();
        }

        public override void AfterAddTeam(Team team)
        {
            base.AfterAddTeam(team);

            QueryDataStore.AddTeam(team);
        }
    }
}
