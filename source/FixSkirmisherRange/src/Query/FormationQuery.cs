﻿using TaleWorlds.MountAndBlade;

namespace FixSkirmisherRange.Query
{
    public class FormationQuery
    {
        public Formation Formation { get; }

        public QueryData<float> MissileRange { get; }

        public FormationQuery(Formation formation)
        {
            Formation = formation;
            MissileRange = new QueryData<float>(() =>
            {
                if (formation.CountOfUnits == 0)
                    return 0.0f;
                var rangedCount = (int)(formation.CountOfUnits *
                                   (formation.QuerySystem.RangedUnitRatio +
                                    formation.QuerySystem.RangedCavalryUnitRatio));
                if (rangedCount == 0)
                    return 0;
                float sum = 0.0f;
                formation.ApplyActionOnEachUnit(agent =>
                {
                    sum += agent.MaximumMissileRange;
                });
                return sum / rangedCount;
            }, 10);
        }
    }
}
