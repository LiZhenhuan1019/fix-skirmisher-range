﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Reflection;
using TaleWorlds.Core;
using TaleWorlds.MountAndBlade;

namespace FixSkirmisherRange
{
    public class Patch_MissionWeapon
    {
        public static bool Patch(Harmony harmony)
        {
            try
            {
                harmony.Patch(
                    typeof(MissionWeapon).GetMethod("GatherInformationFromWeapon",
                        BindingFlags.Instance | BindingFlags.Public),
                    prefix: new HarmonyMethod(typeof(Patch_MissionWeapon).GetMethod(
                        nameof(Prefix_GatherInformationFromWeapon), BindingFlags.Static | BindingFlags.Public)));
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool Prefix_GatherInformationFromWeapon(
            MissionWeapon __instance,
            List<WeaponComponentData> ____weapons,
            out bool weaponHasMelee,
            out bool weaponHasShield,
            out bool weaponHasPolearm,
            out bool weaponHasNonConsumableRanged,
            out bool weaponHasThrown,
            out WeaponClass rangedAmmoClass)
        {
            weaponHasMelee = false;
            weaponHasShield = false;
            weaponHasPolearm = false;
            weaponHasNonConsumableRanged = false;
            weaponHasThrown = false;
            rangedAmmoClass = WeaponClass.Undefined;
            foreach (WeaponComponentData weapon in ____weapons)
            {
                weaponHasMelee = weaponHasMelee || weapon.IsMeleeWeapon;
                weaponHasShield = weaponHasShield || weapon.IsShield;
                weaponHasPolearm = weapon.IsPolearm;
                if (weapon.IsRangedWeapon)
                {
                    weaponHasThrown = weapon.IsConsumable;
                    weaponHasNonConsumableRanged = true;
                    rangedAmmoClass = weapon.AmmoClass;
                }
            }

            return false;
        }
    }
}
