﻿using HarmonyLib;
using System;
using System.Reflection;
using FixSkirmisherRange.Query;
using TaleWorlds.Engine;
using TaleWorlds.Library;
using TaleWorlds.MountAndBlade;

namespace FixSkirmisherRange
{
    public class Patch_MovementOrder
    {
        public static bool Patch(Harmony harmony)
        {
            try
            {
                harmony.Patch(
                    typeof(MovementOrder).GetMethod("GetPositionAux",
                        BindingFlags.Instance | BindingFlags.NonPublic),
                    prefix: new HarmonyMethod(typeof(Patch_MovementOrder).GetMethod(
                        nameof(Prefix_GetPositionAux), BindingFlags.Static | BindingFlags.Public)));
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool Prefix_GetPositionAux(MovementOrder __instance, ref WorldPosition __result, Formation f)
        {

            switch (__instance.OrderType)
            {
                case OrderType.Advance:
                    FormationQuerySystem querySystem = f.QuerySystem;
                    FormationQuerySystem closestEnemyFormation = querySystem.ClosestEnemyFormation;
                    if (closestEnemyFormation == null)
                    {
                        __result = f.OrderPosition;
                        return false;
                    }
                    WorldPosition medianPosition1 = closestEnemyFormation.MedianPosition;
                    if (querySystem.IsRangedFormation || querySystem.IsRangedCavalryFormation)
                    {
                        Vec2 directionAux = GetDirectionAux(f);
                        medianPosition1.SetVec2(medianPosition1.AsVec2 - directionAux * QueryDataStore.Get(f).MissileRange.Value);
                    }
                    else
                    {
                        Vec2 vec2 = (closestEnemyFormation.AveragePosition - f.QuerySystem.AveragePosition).Normalized();
                        float num = 2f;
                        if (closestEnemyFormation.FormationPower < f.QuerySystem.FormationPower * 0.200000002980232)
                            num = 0.1f;
                        medianPosition1.SetVec2(medianPosition1.AsVec2 - vec2 * num);
                    }
                    __result = medianPosition1;
                    return false;
                default:
                    return true;
            }
        }

        private static Vec2 GetDirectionAux(Formation f)
        {
            FormationQuerySystem querySystem = f.QuerySystem;
            FormationQuerySystem closestEnemyFormation = querySystem.ClosestEnemyFormation;
            return closestEnemyFormation == null ? Vec2.One : (closestEnemyFormation.MedianPosition.AsVec2 - querySystem.AveragePosition).Normalized();
        }
    }
}
